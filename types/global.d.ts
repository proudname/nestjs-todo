import { MongooseModuleOptions } from '@nestjs/mongoose/dist/interfaces/mongoose-options.interface';

export type SuccessResponse<T=any> = {
  ok: 1,
  data?: T
}

export type FailResponse = {
  ok: 0,
  message: string,
  code: number
}

export type DefaultResponse<T=any> = SuccessResponse<T>|FailResponse;
export type DefaultAsyncResponse<T=any> = Promise<DefaultResponse<T>>;

export type AppConfig = {
  mongoUrl: string,
  mongoOptions: MongooseModuleOptions,
  appPort: number
}