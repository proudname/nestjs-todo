export enum TaskErrorCode {
  UNKNOWN_ERROR,
  DB_QUERY_ERROR
}