import { ApiProperty } from '@nestjs/swagger';

export class CreateTaskDto {

  @ApiProperty()
  readonly text: string;

}