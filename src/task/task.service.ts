import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Task } from './schemas/task.schema';
import { CreateTaskDto } from './dto/create-task.dto';

@Injectable()
export class TaskService {
  constructor( @InjectModel(Task.name) private taskModel: Model<Task> ) {}


  async findAll(): Promise<Task[]> {
    return this.taskModel.find().select('text _id').maxTimeMS(10000);
  }

  async create(createTaskDto: CreateTaskDto): Promise<Task> {
    return this.taskModel.create(createTaskDto);
  }

  async delete(id: string): Promise<{ ok: number, deletedCount: number }> {
    const { ok, deletedCount } = await this.taskModel.deleteOne({_id: id}).maxTimeMS(10000);
    return { ok, deletedCount };
  }

}
