import { Body, Controller, Delete, Get, Param, Post, Res } from '@nestjs/common';
import { TaskService } from './task.service';
import { CreateTaskDto } from './dto/create-task.dto';
import { TaskErrorCode } from './task.enum';
import { ApiOperation, ApiResponse } from '@nestjs/swagger';
import { Response } from 'express';

@Controller('/api/tasks')
export class TaskController {

  constructor(private readonly taskService: TaskService) {
  }

  @Get('/v1/list')
  @ApiOperation({
    summary: 'Get the tasks list'
  })
  @ApiResponse({
    status: 200
  })
  @ApiResponse({
    status: 500
  })
  async getActualTasks(@Res() res: Response): Promise<void> {
    try {
      const tasks = await this.taskService.findAll();
      res.json({ ok: 1, data: tasks });
    } catch (e) {
      res.status(500)
        .json({ ok: 0, message: 'Error while getting tasks. Please try later.', code: TaskErrorCode.UNKNOWN_ERROR })
    }
  }

  @Post('/v1/new')
  @ApiOperation({
    summary: 'Create new task'
  })
  @ApiResponse({
    status: 200
  })
  @ApiResponse({
    status: 500
  })
  async createTask(@Body() createTaskDto: CreateTaskDto, @Res() res: Response): Promise<void> {
    try {
      const createTaskResult = await this.taskService.create(createTaskDto);
      res.json({ ok: 1, data: createTaskResult})
    } catch (e) {
      res.status(500)
        .json({ ok: 0, message: 'Error while create task. Please try later.', code: TaskErrorCode.UNKNOWN_ERROR });
    }
  }

  @Delete('/v1/delete/:id')
  @ApiOperation({
    summary: 'Delete the task'
  })
  @ApiResponse({
    status: 200
  })
  @ApiResponse({
    status: 500
  })
  async removeTask(@Param('id') id: string, @Res() res: Response): Promise<void> {
    try {
      const { ok, deletedCount } = await this.taskService.delete(id);
      if (!ok) {
        res.status(500)
          .json({ ok: 0, message: 'Error while delete task. Please try later.', code: TaskErrorCode.DB_QUERY_ERROR })
        return;
      }
      if (deletedCount < 1) {
        res.status(500)
          .json({ ok: 0, message: 'Task not found', code: TaskErrorCode.UNKNOWN_ERROR });
        return;
      }
      res.json({ ok: 1 });
    } catch (e) {
      res.status(500)
        .json({ ok: 0, message: 'Error while getting tasks. Please try later.', code: TaskErrorCode.UNKNOWN_ERROR })
    }
  }


}
