import { AppConfig } from '../types/global';

const appConfig: AppConfig = {
  mongoUrl: 'mongodb://localhost:27017/todos_example',
  mongoOptions: {},
  appPort: 3000
}

export default appConfig;