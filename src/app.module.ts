import { Module } from '@nestjs/common';
import { TaskModule } from './task/task.module';
import { MongooseModule } from '@nestjs/mongoose';
import appConfig from './app.config';

@Module({
  imports: [
    MongooseModule.forRoot(appConfig.mongoUrl, appConfig.mongoOptions),
    TaskModule
  ]
})
export class AppModule {}
