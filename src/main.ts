import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import appConfig from './app.config';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';


async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const options = new DocumentBuilder()
    .setTitle('Todo app')
    .setDescription('Todo test app')
    .setVersion('1.0')
    .addTag('todo')
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api', app, document);
  await app.listen(appConfig.appPort);
}
bootstrap();
